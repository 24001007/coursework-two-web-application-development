# Degree programmes

- identification code
- name
- learning outcomes
- a number of exit awards
- led by one academic

# Module

- identification code
- name
- number of hours
- learning outcomes
- number of credits
- One or more assessments
- led by one academic
- is time-tabled for one or more time-slots (one slot each week, every two
  weeks, irregular slots, etc)
- Belongs to one or more degree programmes

# Assessments

- title
- number
- learning outcomes
- volume
- weighting (all weightings must add up to 100%)
- submission date

# Time slots

- room
- academic

# Exit awards

- Required module credits
- name

# User Stories

- An academic creates a new module that is added to one or more degree
  programme(s).
- An academic creates a new degree programme and adds existing modules or new
  modules to it.
- Academics schedule the dates of the assessments for modules that they are
  leading in the current academic year.
- Head-of-department schedules the module time-slots for an academic year.

## Scenario

The University runs a web-based academic management and tracking application,
which in its current form tracks all academics research activities. It is your
task to extend this application so that it also tracks teaching-related
activities. This extension must support the following information:The core
concept for teaching is **the module**. Modules have an _identification code_,
_name_, _number of hours_, _learning outcomes_, and _number of credits_. Each
module has _one or more_ **assessments** assigned to it. Each assessment has a
_title_, _number_, _learning outcomes_ it covers, _volume_, and _weighting_. The
weightings of all assessments of a module must sum to 100%. In each academic
year a module is led by exactly one _academic_. In each academic year a module
is _time-tabled_ for one or more **time-slots**. Some modules have one slot each
week, some have multiple slots each week, and some have irregular slots
throughout the semester or year. Each slot is taught by one _academic_. _Regular
slots_ are frequently taught by the same _academic_ each time, but there are
also modules with regular slots that are taught by a different academic each
time. Each time-slot is also assigned a room. As with academics, regular slots
are frequently taught in the same room, but can also be in different rooms. In
each academic year each assessment is given one _submission date_. Modules are
organised under **degree programmes**. Each module belongs to one or more degree
programmes. A degree programme has an _identification code_, _name_, and
_learning outcomes_. A degree programme also has a number of **exit awards**
that define the degree that the student is given if they complete a certain
number of module credits. A degree programme is led by exactly one academic.

The core concept for teaching is the module. **Modules** have an _identification
code_, _name_, _number of hours_, _learning outcomes_, and _number of credits_.
_Each module has one or more assessments assigned to it_. Each **assessment**
has a _title_, _number_, _learning outcomes_ it covers, _volume_, and
_weighting_. The weightings of all assessments of a module must sum to 100%.

In each **academic year** a **module** is _led by exactly one academic_. In each
**academic year** a **module** is _time-tabled_ for _one or more time-slots_.
Some **modules** have one slot each week, some have multiple slots each week,
and some have irregular slots throughout the semester or year. Each **slot** is
taught by one _academic_. Regular slots are frequently taught by the same
academic each time, but there are also modules with regular slots that are
taught by a different academic each time. Each time-slot is also assigned a
room. As with academics, regular slots are frequently taught in the same _room_,
but can also be in different rooms. In each academic year each **assessment** is
given _one submission date_.

**Modules** are _organised under degree programmes_. Each **module** _belongs to
one or more degree programmes_. A **degree programme** has an _identification
code_, _name_, and _learning outcomes_. A degree programme also has a number of
_exit awards_ that define the degree that the student is given if they complete
a certain number of _module credits_. A **degree programme** is _led by exactly
one academic_.
