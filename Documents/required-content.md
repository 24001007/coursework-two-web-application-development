1. Modelling techniques - Description of the modelling techniques used in your
   design and how they work together.
2. User modelling - Use-case models for the user stories listed above including
   explanatory text for each use-case.
3. Structure modelling - Class diagram modelling the problem space described
   above and Explanatory text for the class diagram.
4. Process modelling - Activity diagrams showing the application flow for the
   user stories and problem space described above and explanatory text for each
   activity diagram.
5. Design, Develop, deploy - Design and develop interfaces for one of your
   extension features (JavaScript, Html, JSON and CSS); Write a deployment plan
   and demonstrate the plan being actioned.
